from django.db import models

# Create your models here.
class Tutor(models.Model):

    class Meta:
        db_table = 'tutor' # define your custom name

    tutor_id = models.AutoField(primary_key=True)
    tutor_name = models.CharField(max_length=50)
    tutor_email = models.CharField(max_length=50)

    def __str__(self):
        return self.tutor_name
