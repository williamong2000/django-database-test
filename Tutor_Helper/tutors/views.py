from django.shortcuts import render
from .models import Tutor

# Create your views here.
def tutor_list(request):
    tutors = Tutor.objects.all()
    return render(request,'tutors/tutor_list.html',{ 'tutors':tutors })
